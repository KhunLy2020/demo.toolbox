﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ToolBox.ConsoleUtils
{
    public static class Utils
    {
        public static string Question(string question, int duree = 0)
        {
            if(duree == 0)
            {
                Console.WriteLine(question);
            }
            else 
            {
                foreach (char c in question)
                {
                    Console.Write(c);
                    Thread.Sleep(duree);
                }
                Console.WriteLine();
            }
            return Console.ReadLine();
        }

        public static string StringList(IEnumerable collection)
        {
            string result = "[";
            IEnumerator enumerator = collection.GetEnumerator();
            bool next = enumerator.MoveNext();
            while (next)
            {
                if (enumerator.Current is IEnumerable)
                {
                    result += StringList(enumerator.Current as IEnumerable);
                }
                else
                {
                    result += enumerator.Current.ToString();
                }
                next = enumerator.MoveNext();
                result += next ? "," : "";
            }
            result += "]";
            enumerator.Reset();
            return result;
        }

        public static void WriteList(IEnumerable collection)
        {
            Console.WriteLine(StringList(collection)); 
        }
    }
}
