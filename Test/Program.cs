﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ConsoleUtils;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            List<List<int>> list = new List<List<int>>
            {
                new List<int>{1,2,3},
                new List<int>{4,5,6},
                new List<int>{7,8,9},
            };
            Utils.WriteList(list);
            Console.ReadKey();
        }
    }
}
